from flask import Flask, render_template
import requests
from flask_sqlalchemy import SQLAlchemy
import os
from sys import platform


def init_database(var_app):
    mini_path = os.getcwd()
    if platform == "win32":
        path = f'sqlite:///{mini_path}\\database\\working_dir\\cinematech.db'
    else:
        path = f'sqlite:///{mini_path}/database/working_dir/cinematech.db'
    var_app.config['SQLALCHEMY_DATABASE_URI'] = path
    var_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    temp_db = SQLAlchemy(var_app)
    temp_db.init_app(app)
    temp_db.create_all()
    return temp_db


app = Flask(__name__)
db = init_database(var_app=app)


class Movie(db.Model):
    __tablename__ = "Movie"

    id = db.Column(db.Integer, primary_key=True)
    imdb_id = db.Column(db.String, unique=True, nullable=False)

    def __repr__(self):
        return f'<Movie {self.id}> -- Imdb: {self.imdb_id}'


def fill_database():
    var = [
        "tt1191111",
        "tt0056869",
        "tt0099871",
        "tt0103873",
        "tt0054443",
        "tt0070047",
        "tt0382628",
        "tt0057129",
        "tt1179904",
        "tt0093177",
        "tt0364385",
        "tt0237534",
        "tt0081505",
        "tt0410764",
        "tt0055830",
        "tt0384537",
        "tt0166924",
        "tt0054215",
        "tt0144084",
        "tt0062622",
        "tt3464902",
        "tt2866360",
        "tt0898367",
        "tt0448134"
    ]
    for imdb_id in var:
        tmp = Movie(imdb_id=imdb_id)
        db.session.add(tmp)
    db.session.commit()


db.create_all()


@app.route('/')
def get():
    films = []
    mylist = Movie.query.all()
    for imdb_code in mylist:
        headers = {"Content-Type": "application/json"}
        url = "http://www.omdbapi.com/?apikey=1a61de53&i=" + imdb_code.imdb_id
        r = requests.get(url, headers=headers).json()

        films.append(r)
    return render_template('index.html', films=films)


if __name__ == '__main__':
    app.run()
